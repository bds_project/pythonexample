#Not tested yet

#To get sytax highlighting you will need to download spark http://spark.apache.org/downloads.html
#Choose any prebuilt version with any hadoop version
#Add the python dependencies to your python interpreter spark-1.6.1-bin-hadoop2.6/python
#To run you will have to use the spark submit script
#.spark-1.6.1-bin-hadoop2.6/bin/spark-submit Example.py

__author__ = 'islam'
from pySpark import SparkContext
from pyspark.sql import SQLContext

#Create a spark context
sc = SparkContext("local", "BDProject")
sqlContext = SQLContext(sc)

#Reading tables that we have, each table is really a directory
table = sqlContext.read.parquet("/Users/islam/IdeaProjects/SparkExamples/ParquetTables/menu")
#Prints a small subset of the data
table.show
#Print the schema so we know what datatypes we are working with
table.printSchema

#You can transform data on the dataframe through referencing columns
table.select(table['Price'] * 5).show()

#We have a sql api if we want to do transformations through sql
table.registerTempTable("menus")

#Here you can run sql over the new temp table
sqlContext.sql("Select * from menus").show()

#However python doesn't have support for higher user defined functions on the table
#So all manipulation will have to treat results as an RDD
#An RDD is basically an array distributed over multiple machines
sampleRDD = sqlContext.sql("Select * from menus")

#On the rdd you can run map and reduce functions
allConcat = sampleRDD\
    .map(lambda x: x.getString(0))\
    .reduce(lambda a,b: a + b)

#If the data fits on a single machines memory it can be collected into an array and worked on directly
aList = sampleRDD.collect()

#More info at http://spark.apache.org/docs/latest/sql-programming-guide.html#sql